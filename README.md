# json-to-jsdoc-converter

# [Web demo](https://json-to-jsdoc-converter-webui.000webhostapp.com/)

## How to use:

### Import dependencies

```javascript
import { JsonToJsdocConverter } from "json-to-jsdoc-converter";
```

#### Initialize converter

```javascript
const converter = new JsonToJsdocConverter();
```

#### Provide some json

```json
 {
  "id": 1,
  "name": "Super Mario",
  "age": null,
  "cars": [
    "honda",
    "bmw"
  ],
  "structure": {
    "logo": ""
  },
  "level1": {
    "alias": "testLevel",
    "level2": {
      "score": 12,
      "item": {},
      "level3": null
    }
  }
}
```

#### Pass the json to the convert method

```javascript
const json = JSON.stringify(jsonMock1);
const jsDoc = converter.convert(json, { typesPrefix: "", typesSuffix: "Type" }); //you can set type name rules by passing optional config
```

#### Use the jsDoc output

```text
/**
* @typedef Type
* @property {number} id
* @property {string} name
* @property {any} [age]
* @property {string[]} cars
* @property {StructureType} structure
* @property {Level1Type} level1
*/

/**
* @typedef StructureType
* @property {string} logo
*/

/**
* @typedef Level1Type
* @property {string} alias
* @property {Level1Level2Type} level2
*/

/**
* @typedef Level1Level2Type
* @property {number} score
* @property {object} item
* @property {any} [level3]
*/
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://gitlab.com/nvidia1997/json-to-jsdoc-converter/LICENSE)