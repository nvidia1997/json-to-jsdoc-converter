import {JsonToJsdocConverter} from "../../public";
import jsonMock1 from "./json-mocks/json-mock1.json";

function main() {
    const jsonToJsdocConverter = new JsonToJsdocConverter();
    const json: string = JSON.stringify(jsonMock1);
    const jsDoc: string = jsonToJsdocConverter.convert(json, {typesPrefix: "Alien", typesSuffix: ""});

    // @ts-ignore
    // tslint:disable-next-line:no-console
    console.log(jsDoc);
}

main();