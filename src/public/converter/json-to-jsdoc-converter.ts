// @ts-ignore
import flatten from "flat";
import {CustomType, Property} from "./internal";

interface IConvertConfig {
    typesPrefix?: string;
    typesSuffix?: string;
}

export class JsonToJsdocConverter {
    private _convertConfig: IConvertConfig = {};

    private _flat(obj: object): object {
        return flatten(obj, {safe: true,});
    }

    /**
     * @returns Js Doc
     */
    public convert(json: string, convertConfig?: IConvertConfig): string {
        this._convertConfig = convertConfig || this._convertConfig;

        const obj = JSON.parse(json);
        const flattenedObj = this._flat(obj);

        return this._parseJsdoc(flattenedObj);
    }

    private _parseJsdoc(flattenedObj: object): string {
        const _props = this._parseProps(flattenedObj);
        const _types = this._createTypes(_props);
        const jsDocTypes = _types.map(t => this._createJsDocForType(t, _types));

        return jsDocTypes
            .join("\n\n");
    }

    private _createJsDocForType(type: CustomType, types: CustomType[]): string {
        const _lines = [];

        _lines.push(`/**`);
        _lines.push(`* @typedef ${type.name}`);
        _lines.push(this._createPropsJsDoc(type, types));
        _lines.push(`*/`);

        return _lines.join("\n");
    }

    private _createJsDocPropName(prop: Property): string {
        return prop.isOptional
            ? `[${prop.name}]`
            : prop.name;
    }

    private _createJsDocPropType(prop: Property): string {
        return prop.isOptional
            ? Property.TYPES.ANY
            : prop.type;
    }

    private _createPropsJsDoc(type: CustomType, types: CustomType[]): string {
        const _lines: string[] = [];

        for (const prop of type.props) {
            const _type = prop.isCustomType
                ? types.find(t => t.props.some(p => p.parentPath === prop.path))
                : undefined;

            const _propType = _type
                ? _type.name
                : this._createJsDocPropType(prop);

            const _line = `* @property {${_propType}} ${this._createJsDocPropName(prop)}`;

            _lines.push(_line);
        }

        return _lines.join("\n");
    }

    private _createTypes(props: Property[]): CustomType[] {
        const parentPaths = new Set(props.map(p => p.parentPath));
        const types: CustomType[] = [];

        parentPaths.forEach((path) => {
            const _props = props.filter(p => p.parentPath === path);
            const _type = new CustomType({
                path,
                props: _props,
                typesPrefix: this._convertConfig.typesPrefix,
                typesSuffix: this._convertConfig.typesSuffix
            });
            types.push(_type);
        });

        return types;
    }

    private _parseProps(flattenedObj: object): Property[] {
        const props: Property[] = [];
        // tslint:disable-next-line:forin
        for (const propPath in flattenedObj) {
            const property = new Property({
                path: propPath,
                // @ts-ignore
                value: flattenedObj[propPath],
            });
            props.push(property);

            if (property.parentPath !== "" && !props.some(p => p.path === property.parentPath)) {
                props.push(new Property({
                    path: property.parentPath,
                    value: {}
                }));
            }
        }

        return props;
    }

}

