export class Property {
    static TYPES = {
        ANY: "any",
    };

    private readonly _value: any;
    private readonly _path: string;

    constructor({path, value}: { path: string, value: any }) {
        this._path = path;
        this._value = value;
    }

    public get path(): string {
        return this._path;
    }

    public get parentPath(): string {
        const _paths = [...this.paths];
        _paths.pop();

        return _paths.join(".");
    }

    public get paths(): string[] {
        return this.path.split(".");
    }

    public get name(): string {
        const _paths = this.paths;
        return _paths[_paths.length - 1];
    }

    private get _arrayType(): string {
        const item0Type = typeof this._value[0];

        if (this._value.length === 0) {
            return Property.TYPES.ANY;
        }

        if (this._value.some((element: any) => typeof element !== item0Type)) {
            return Property.TYPES.ANY;
        }

        return item0Type;
    }

    private _isOptional(value: any): boolean {
        return value === null || value === undefined;
    }

    public get isOptional(): boolean {
        return this._isOptional(this._value);
    }

    public get isCustomType(): boolean {
        return this.type === "object";
    }

    public get type(): string {
        if (Array.isArray(this._value)) {
            return `${this._arrayType}[]`;
        }

        return this.isOptional
            ? Property.TYPES.ANY
            : typeof this._value;
    }
}