import {StringHelper} from "../utils/string-helper";
import {Property} from "./internal";

interface ICustomTypeConstructorParams {
    path: string,
    props: Property[],
    typesPrefix?: string,
    typesSuffix?: string
}

export class CustomType {
    private readonly _typesPrefix: string;
    private readonly _typesSuffix: string;
    private readonly _path: string;
    private readonly _props: Property[];

    constructor({
                    path,
                    props,
                    typesPrefix = "",
                    typesSuffix = ""
                }: ICustomTypeConstructorParams) {
        this._path = path;
        this._props = props;

        this._typesPrefix = typesPrefix;
        this._typesSuffix = !typesSuffix && !typesPrefix ? "Type" : typesSuffix;
    }

    public get path(): string {
        return this._path;
    }

    public get parentPath(): string {
        const _paths = [...this.paths];
        _paths.pop();

        return _paths.join(".");
    }

    public get paths(): string[] {
        return this.path.split(".");
    }

    public get name(): string {
        return this._typesPrefix + this.paths
            .map(p => StringHelper.capitalize(p))
            .join("") + this._typesSuffix;
    }

    public get props(): Property[] {
        return this._props;
    }

}