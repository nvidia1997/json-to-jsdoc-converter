export class StringHelper {
    static capitalize(text: string): string {
        if (typeof text !== 'string' || !text) {
            return text;
        }

        return text.charAt(0).toUpperCase() + text.slice(1);
    };
}