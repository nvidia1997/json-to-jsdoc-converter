import {JsonToJsdocConverter} from "../src/public";
import jsonMock1 from "../src/private/examples/json-mocks/json-mock1.json";

test("string validator-1", () => {
    const jsonToJsdocConverter = new JsonToJsdocConverter();
    const json: string = JSON.stringify(jsonMock1);
    const jsDoc: string = jsonToJsdocConverter.convert(json);

    const testCase1 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef Type`),
        jsDoc.includes(`* @property {number} id`),
        jsDoc.includes(`* @property {string} name`),
        jsDoc.includes(`* @property {any} [age]`),
        jsDoc.includes(`* @property {string[]} cars`),
        jsDoc.includes(`* @property {StructureType} structure`),
        jsDoc.includes(`* @property {Level1Type} level1`),
        jsDoc.includes(`*/`),
    ];

    const testCase2 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef StructureType`),
        jsDoc.includes(`* @property {string} logo`),
        jsDoc.includes(`*/`),
    ];

    const testCase3 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef Level1Type`),
        jsDoc.includes(`* @property {string} alias`),
        jsDoc.includes(`* @property {Level1Level2Type} level2`),
        jsDoc.includes(`*/`),
    ];

    const testCase4 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef Level1Level2Type`),
        jsDoc.includes(`* @property {number} score`),
        jsDoc.includes(`* @property {object} item`),
        jsDoc.includes(`* @property {any} [level3]`),
        jsDoc.includes(`*/`),
    ];

    const isJsDocValid = [...testCase1, ...testCase2, ...testCase3, ...testCase4]
        .every(testCase => testCase === true);

    expect(isJsDocValid).toBeTruthy();
});


test("string validator-2 prefixes and suffixes", () => {
    const jsonToJsdocConverter = new JsonToJsdocConverter();
    const json: string = JSON.stringify(jsonMock1);
    const jsDoc: string = jsonToJsdocConverter.convert(json, {typesPrefix: "Alien", typesSuffix: ""});

    const testCase1 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef Alien`),
        jsDoc.includes(`* @property {number} id`),
        jsDoc.includes(`* @property {string} name`),
        jsDoc.includes(`* @property {any} [age]`),
        jsDoc.includes(`* @property {string[]} cars`),
        jsDoc.includes(`* @property {AlienStructure} structure`),
        jsDoc.includes(`* @property {AlienLevel1} level1`),
        jsDoc.includes(`*/`),
    ];

    const testCase2 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef AlienStructure`),
        jsDoc.includes(`* @property {string} logo`),
        jsDoc.includes(`*/`),
    ];

    const testCase3 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef AlienLevel1`),
        jsDoc.includes(`* @property {string} alias`),
        jsDoc.includes(`* @property {AlienLevel1Level2} level2`),
        jsDoc.includes(`*/`),
    ];

    const testCase4 = [
        jsDoc.includes(`/**`),
        jsDoc.includes(`* @typedef AlienLevel1Level2`),
        jsDoc.includes(`* @property {number} score`),
        jsDoc.includes(`* @property {object} item`),
        jsDoc.includes(`* @property {any} [level3]`),
        jsDoc.includes(`*/`),
    ];

    const isJsDocValid = [...testCase1, ...testCase2, ...testCase3, ...testCase4]
        .every(testCase => testCase === true);

    expect(isJsDocValid).toBeTruthy();
});